package com.example.tp33;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;
import static com.example.tp33.WeatherDbHelper._ID;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    ListView listView;
    ListView cityList;
    public static final String SELECTED_CITY = "SELECTED_CITY";
    private static final int CITY_ACTIVITY_REQUEST_CODE = 1;
    Cursor cursor;
    WeatherDbHelper weatherDbHelper;
    CursorAdapteur cursorAdapter;
    private SwipeRefreshLayout mRefreshLayout;


    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRefreshLayout = findViewById(R.id.refresh_layout);
        mRefreshLayout.setOnRefreshListener(this);


        listView = findViewById(R.id.mylist);

        weatherDbHelper = new WeatherDbHelper(this);

        weatherDbHelper.populate();
        cursor = weatherDbHelper.fetchAllCities();

        cursorAdapter = new CursorAdapteur(this, cursor);

        listView.setAdapter(cursorAdapter);

        City city1 = (City) getIntent().getParcelableExtra("city");

        if(city1 != null) {
            weatherDbHelper.addCity(city1);
            cursor = weatherDbHelper.fetchAllCities();
            cursorAdapter.changeCursor(cursor);
            cursorAdapter.notifyDataSetChanged();
        }


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivity(intent);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Cursor cursor = (Cursor)listView.getItemAtPosition(position);
                City city = weatherDbHelper.cursorToCity(cursor);
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra(SELECTED_CITY, city);
                startActivityForResult(intent, 2);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 2 && RESULT_OK == resultCode) {
            City updatedCity = data.getParcelableExtra("city");
            weatherDbHelper.updateCity(updatedCity);
            cursor = weatherDbHelper.fetchAllCities();
            cursorAdapter.changeCursor(cursor);
            cursorAdapter.notifyDataSetChanged();

        }

    }
        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("MainActivity :: onResume()");
        cursor = weatherDbHelper.fetchAllCities();
        cursorAdapter.changeCursor(cursor);
        cursorAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        if (MainActivity.isOnline(this))
            new miseAjour().execute();
        else {
            Snackbar.make(findViewById(R.id.refresh_layout), "Vérifiez votre connexion internet", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            mRefreshLayout.setRefreshing(false);
        }

    }

    class miseAjour extends AsyncTask <Object,Integer, List<City>> {
        private List<City> myList;

        @Override
        protected void onPreExecute() {
            myList = weatherDbHelper.getAllCities();
            super.onPreExecute();
        }


        @Override
        protected List<City> doInBackground(Object... objects) {
            for (City city : myList) {
                weatherRequest(city);
                weatherDbHelper.updateCity(city);
            }
            return myList;
        }

        @Override
        protected void onPostExecute(List<City> cities) {
            myList = weatherDbHelper.getAllCities();
            cursor = weatherDbHelper.fetchAllCities();
            cursorAdapter.changeCursor(cursor);
            cursorAdapter.notifyDataSetChanged();
            mRefreshLayout.setRefreshing(false);
            super.onPostExecute(cities);

        }


        private void weatherRequest(City city) {
            HttpURLConnection urlConnection = null;
            try {
                // prepare url
                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                // send a GET request to the serve
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                // read data
                InputStream inputStream = urlConnection.getInputStream();
                JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                jsonHandler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
        }

    }
}


