package com.example.tp33;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


public class CityActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textLabelTemperature, textTemperature, textLabelHumidity, textHumdity, textLabelWind, textWind, textLabelCloudiness, textCloudiness, textLabelLastUpdate, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City city;
    private SwipeRefreshLayout mRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        //city = (City) getIntent().getParcelableExtra(City.TAG);
        city = getIntent().getParcelableExtra(MainActivity.SELECTED_CITY);

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textLabelTemperature = (TextView) findViewById(R.id.labelTemperature);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textLabelHumidity = (TextView) findViewById(R.id.labelHumidity);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textLabelWind = (TextView) findViewById(R.id.labelWind);
        textWind = (TextView) findViewById(R.id.editWind);
        textLabelCloudiness = (TextView) findViewById(R.id.labelCloudiness);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLabelLastUpdate = (TextView) findViewById(R.id.labelLastUpdate);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);

        mRefreshLayout = findViewById(R.id.refresh_layout_city);
        mRefreshLayout.setOnRefreshListener(this);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener((View.OnClickListener) this);

    }

    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        System.out.println("back");

        Intent intent = new Intent();
        intent.putExtra("city", city);
        setResult(RESULT_OK, intent);
        finish();

        super.onBackPressed();
    }

    private void updateView() {

        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature() + " °C");
        textHumdity.setText(city.getHumidity() + " %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getHumidity() + " %");
        textLastUpdate.setText(city.getLastUpdate());

        if (city.getIcon() != null && !city.getIcon().isEmpty()) {
            Log.d(TAG, "icon=" + "icon_" + city.getIcon());
            imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/" + "icon_" + city.getIcon(), null, getPackageName())));
            imageWeatherCondition.setContentDescription(city.getDescription());
        }

    }

    @Override
    public void onClick(View v) {

        if (MainActivity.isOnline(CityActivity.this)) {
            mRefreshLayout.setRefreshing(true);
            new miseAjour().execute();


        } else {
            Snackbar.make(findViewById(R.id.refresh_layout_city), "No connexion", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }

    }

    @Override
    public void onRefresh() {
        if (MainActivity.isOnline(CityActivity.this)) {
            new miseAjour().execute();
        } else {
            Snackbar.make(findViewById(R.id.refresh_layout_city), "Vérifiez votre connexion internet", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }

    }

    class miseAjour extends AsyncTask<Object, Integer, City> {

        @Override
        protected City doInBackground(Object... objects) {
            HttpURLConnection urlConnection = null;
            try {
                // prepare url
                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                // send a GET request to the serve
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                // read data
                InputStream inputStream = urlConnection.getInputStream();
                JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                jsonHandler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            return city;
        }

        @Override
        protected void onPostExecute(City city) {
            updateView();
            mRefreshLayout.setRefreshing(false);
            super.onPostExecute(city);

        }
    }
}