package com.example.tp33;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewCityActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText textName, textCountry;
    City city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);

        Intent intent = getIntent();
        but.setOnClickListener((View.OnClickListener)this);
    }

    @Override
    public void onClick(View v) {
        city = new City( textName.getText().toString(), textCountry.getText().toString(),"14°C","46","12","NW","26","50d","cold","Sat Apr 12:20:05:30  GMT+03:20 2019");
        Intent intent = new Intent(NewCityActivity.this,MainActivity.class);
        intent.putExtra("city",city);
        Toast.makeText(this, "AJOUTER", Toast.LENGTH_SHORT).show();
        startActivity(intent);
        finish();
    }
}
