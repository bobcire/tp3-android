package com.example.tp33;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;


class CursorAdapteur extends CursorAdapter {

    public CursorAdapteur(Context context, Cursor cursor){
        super(context,cursor,0);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        TextView textViewCity = (TextView) view.findViewById(R.id.cName);
        String CityName = cursor.getString( cursor.getColumnIndex( WeatherDbHelper.COLUMN_CITY_NAME ) );
        textViewCity.setText(CityName);

        TextView textViewCountry = (TextView) view.findViewById(R.id.cCountry);
        String  CountryName = cursor.getString( cursor.getColumnIndex( WeatherDbHelper.COLUMN_COUNTRY ) );
        textViewCountry.setText(CountryName);

        TextView textViewTemp = (TextView) view.findViewById(R.id.temperature);
        String Temperature = cursor.getString(cursor.getColumnIndex(WeatherDbHelper.COLUMN_TEMPERATURE));
        textViewTemp.setText(Temperature);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageViewRow);
        String Img = cursor.getString(cursor.getColumnIndex(WeatherDbHelper.COLUMN_ICON));
        //int id = context.getResources().getIdentifier("com.example.tp3:drawable/"+"icon_" + Img , null, null);

        //imageView.setImageResource(id);

        imageView.setImageDrawable(context.getResources().getDrawable(context.getResources()
                .getIdentifier("@drawable/"+"icon_" + Img, null, context.getPackageName())));
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // R.layout.list_row is your xml layout for each row
        return LayoutInflater.from(context).inflate(R.layout.row, parent, false);

    }

}
