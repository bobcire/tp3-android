package com.example.tp33;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WeatherDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WeatherDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "weather.db";

    public static final String TABLE_NAME = "weather";

    public static final String _ID = "_id";
    public static final String COLUMN_CITY_NAME = "city";
    public static final String COLUMN_COUNTRY = "country";
    public static final String COLUMN_TEMPERATURE = "temperature";
    public static final String COLUMN_HUMIDITY = "humidity";
    public static final String COLUMN_WIND_SPEED = "windSpeed";
    public static final String COLUMN_WIND_DIRECTION = "windDirection";
    public static final String COLUMN_CLOUDINESS = "cloudiness";
    public static final String COLUMN_ICON = "icon";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_LAST_UPDATE = "lastupdate";

    public WeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER , " +

                COLUMN_CITY_NAME + " TEXT NOT NULL, " +
                COLUMN_COUNTRY + " TEXT NOT NULL, " +
                COLUMN_TEMPERATURE + " INTEGER, " +
                COLUMN_HUMIDITY+ " TEXT, " +
                COLUMN_WIND_SPEED+ " TEXT, " +
                COLUMN_WIND_DIRECTION+ " TEXT, " +
                COLUMN_CLOUDINESS+ " TEXT, " +
                COLUMN_DESCRIPTION+ " TEXT, " +
                COLUMN_ICON+ " TEXT, " +
                COLUMN_LAST_UPDATE+ " TEXT, " +

                // To assure the application have just one weather entry per
                // city name and country, it's created a UNIQUE
                " UNIQUE (" + COLUMN_CITY_NAME + ", " +
                COLUMN_COUNTRY + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_BOOK_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME + ";");//drop the old database
        onCreate(db);//run onCreate to get new database
    }


    /**
     * Adds a new city
     * @return  true if the city was added to the table ; false otherwise (case when the pair (city name, country) is
     * already in the data base
     */

    public boolean addCity(City city) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CITY_NAME, city.getName());
        values.put(COLUMN_COUNTRY, city.getCountry());
        values.put(COLUMN_TEMPERATURE, city.getTemperature());
        values.put(COLUMN_HUMIDITY, city.getHumidity());
        values.put(COLUMN_WIND_SPEED, city.getWindSpeed());
        values.put(COLUMN_WIND_DIRECTION, city.getWindDirection());
        values.put(COLUMN_CLOUDINESS, city.getCloudiness());
        values.put(COLUMN_ICON, city.getIcon());
        values.put(COLUMN_DESCRIPTION, city.getDescription());
        values.put(COLUMN_LAST_UPDATE, city.getLastUpdate());

        Log.d(TAG, "adding: "+city.getName()+" with id="+city.getId());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (city name, country)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a city inside the data base
     * @return the number of updated rows
     */

    public int updateCity(City city) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CITY_NAME, city.getName());
        values.put(COLUMN_COUNTRY, city.getCountry());
        values.put(COLUMN_TEMPERATURE, city.getTemperature());
        values.put(COLUMN_HUMIDITY, city.getHumidity());
        values.put(COLUMN_WIND_SPEED, city.getWindSpeed());
        values.put(COLUMN_WIND_DIRECTION, city.getWindDirection());
        values.put(COLUMN_CLOUDINESS, city.getCloudiness());
        values.put(COLUMN_ICON, city.getIcon());
        values.put(COLUMN_DESCRIPTION, city.getDescription());
        values.put(COLUMN_LAST_UPDATE, city.getLastUpdate());

        // updating row
        return db.updateWithOnConflict(TABLE_NAME, values, _ID + " = ?",
                new String[] { String.valueOf(city.getId()) }, CONFLICT_IGNORE);
    }

    /**
     * Returns a cursor on all the cities of the data base
     */
    public Cursor fetchAllCities() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_CITY_NAME+" ASC", null);

        Log.d(TAG, "call fetchAllCities()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    /**
     * Returns a list on all the cities of the data base
     */
    public List<City> getAllCities() {
        // TODO
        List<City> res = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = fetchAllCities();
        while (cursor.moveToNext()) {
            res.add(cursorToCity(cursor));
        }
        db.close();

        return res;
    }

    public void deleteCity(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = ?",
                new String[]{cursor.getString(cursor.getColumnIndex(_ID))});
        db.close();
    }

    public void deleteCity(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

//    public void populate() {
//        Log.d(TAG, "call populate()");
//        addCity(new City("Avignon","France"));
//        addCity(new City("Paris","France"));
//        addCity(new City("Rennes","France"));
//        addCity(new City("Montreal","Canada"));
//        addCity(new City("Fortaleza","Brazil"));
//        addCity(new City("Papeete","French Polynesia"));
//        addCity(new City("Sydney","Australia"));
//        addCity(new City("Seoul","South Korea"));
//        addCity(new City("Bamako","Mali"));
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
//        Log.d(TAG, "nb of rows="+numRows);
//        db.close();
//    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addCity(new City("Avignon","France","8","60","14","NE","60","01d","hot","Wed Jan 16:17:50:00  GMT+02:00 2019"));
        addCity(new City("Paris","France","15","40","22","NE","50","01n","cloud","Tue Feb 12:19:00:00  GMT+01:00 2019"));
        addCity(new City("Rennes","France","14","30","15","NW","40","02d","sunny","Fri Feb 13:18:23:14  GMT+05:23 2019"));
        addCity(new City("Montreal","Canada","16","22","10","SE","30","02n","cold","Sat Mar 17:19:50:12  GMT+04:22 2019"));
        addCity(new City("Fortaleza","Brazil","13","42","8","SE","22","03d","hot","Sun Apr 15:23:45:00  GMT+02:10 2019"));
        addCity(new City("Papeete","French Polynesia","5","53","13","SW","15","03n","sunny","Mon Feb 13:29:50:10  GMT+01:00 2019"));
        addCity(new City("Sydney","Australia","7","26","9","SE","52","04d","cloud","Tue Feb 14:10:05:04  GMT+02:03 2019"));
        addCity(new City("Seoul","South Korea","13","64","7","SW","34","04n","cold","Wed May 13:15:04:02  GMT+03:02 2019"));
        addCity(new City("Bamako","Mali","22","23","5","NW","26","09d","hot","Tue Dec 14:12:04:50  GMT+03:20 2019"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public City cursorToCity(Cursor cursor) {
        City city = new City(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_CITY_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_COUNTRY)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TEMPERATURE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_HUMIDITY)),
                cursor.getString(cursor.getColumnIndex(COLUMN_WIND_SPEED)),
                cursor.getString(cursor.getColumnIndex(COLUMN_WIND_DIRECTION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_CLOUDINESS)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ICON)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_LAST_UPDATE))
        );
        return city;
    }

    public City getCity(int id) {
        //City city ;

        // TODO
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where " + _ID +  " = ?", new String[]{String.valueOf(id)});
        City city = cursorToCity(cursor);
        db.close();

        return city;
    }
}

